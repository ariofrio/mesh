package com.andresriofrio.mesh.util;

public class Bits {

    /**
     * Interprets x as an unsigned integer and returns the result as a long.
     * @param x     A value whose bitwise pattern will be interpreted as an unsigned integer.
     * @return      A signed long with the value of the unsigned integer.
     */
    public static long asUnsigned(int x) {
        return x & 0xFFFFFFFFL;
    }

    /**
     * Interprets x as an unsigned short and returns the result as a integer.
     * @param x     A value whose bitwise pattern will be interpreted as an unsigned short.
     * @return      A signed integer with the value of the unsigned short.
     */
    public static int asUnsigned(short x) {
        return x & 0xFFFF;
    }

    /**
     * Interprets x as an unsigned byte and returns the result as a short.
     * @param x     A value whose bitwise pattern will be interpreted as an unsigned byte.
     * @return      A signed short with the value of the unsigned byte.
     */
    public static short asUnsigned(byte x) {
        return (short) (x & 0xFF);
    }

    public static int toUnsigned(long x) {
        return (int) x;
    }

    public static short toUnsigned(int x) {
        return (short) x;
    }

    public static byte toUnsigned(short x) {
        return (byte) x;
    }

}
