package com.andresriofrio.mesh;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.*;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, true);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public static class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

        public static final int VPN_SERVICE_PREPARED_REQUEST_CODE = 0;
        private static final int DISCOVERABLE_SUCCEEDED_REQUEST_CODE = 1;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            PreferenceManager.getDefaultSharedPreferences(getActivity()).registerOnSharedPreferenceChangeListener(this);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.preferences);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (getActivity() != null) {
                if (key.equals("pref_enable_vpn")) {
                    Preference pref = findPreference(key);
                    if (sharedPreferences.getBoolean(key, false)) {
                        final Intent intent = VpnService.prepare(getActivity());
                        if (intent != null) {
                            startActivityForResult(intent, VPN_SERVICE_PREPARED_REQUEST_CODE);
                        } else {
                            onActivityResult(VPN_SERVICE_PREPARED_REQUEST_CODE, RESULT_OK, null);
                        }
                    } else {
                        String prefix = getActivity().getPackageName();
                        Intent intent = new Intent(getActivity(), VpnService.class);
                        getActivity().stopService(intent);
                    }
                } else if (key.equals("pref_enable_server")) {
                    if (sharedPreferences.getBoolean(key, false)) {
                        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
                        startActivityForResult(discoverableIntent, DISCOVERABLE_SUCCEEDED_REQUEST_CODE);
                    } else {
                        String prefix = getActivity().getPackageName();
                        Intent intent = new Intent(getActivity(), TetheringServerService.class);
                        getActivity().stopService(intent);
                    }
                }
            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (requestCode == VPN_SERVICE_PREPARED_REQUEST_CODE) {
                if (resultCode == RESULT_OK) {
                    String prefix = getActivity().getPackageName();
                    Intent intent = new Intent(getActivity(), VpnService.class)
                            /*.putExtra(prefix + ".ADDRESS", mServerAddress.getText().toString())
                            .putExtra(prefix + ".PORT", mServerPort.getText().toString())
                            .putExtra(prefix + ".SECRET", mSharedSecret.getText().toString())*/;
                    getActivity().startService(intent);
                } else {
                    final SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
                    editor.putBoolean("pref_enable_client", false);
                    editor.commit();
                }
            } else if (requestCode == DISCOVERABLE_SUCCEEDED_REQUEST_CODE) {
                if (resultCode == RESULT_CANCELED) {
                    final SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
                    editor.putBoolean("pref_enable_server", false);
                    editor.commit();
                } else {
                    String prefix = getActivity().getPackageName();
                    Intent intent = new Intent(getActivity(), TetheringServerService.class);
                    getActivity().startService(intent);
                }
            }
        }

        @Override
        public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
            return super.onPreferenceTreeClick(preferenceScreen, preference);
        }
    }

}
