package com.andresriofrio.mesh;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;

public class VpnService extends android.net.VpnService {

    private static final String TAG = VpnService.class.getName();
    private BroadcastReceiver broadcastReceiver;
    private BluetoothDevice device;
    private Object lock = new Object();
    private ParcelFileDescriptor fileDescriptor;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "starting VpnService (tethering client)");

        final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // Create a BroadcastReceiver for ACTION_FOUND
        broadcastReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                // When discovery finds a device
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                    if (device.getName().equals("MeshServer")) {
                        Log.d(TAG, "found a bluetooth server: " + device.toString());

                        bluetoothAdapter.cancelDiscovery();
                        unregisterReceiver(this);

                        VpnService.this.device = device;
                        connect();
//                        lock.notify();
                    }
                }
            }
        };
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        Log.d(TAG, "about to register receiver");
        registerReceiver(broadcastReceiver, filter);
        Log.d(TAG, "starting device discovery");
        bluetoothAdapter.startDiscovery();

//        connect();

        return START_STICKY;
    }

    private void connect() {
        try {
//            Toast.makeText(getApplicationContext(), "attempting to connect to " + device.toString(), Toast.LENGTH_SHORT).show();
            Log.d(TAG, "attempting to connect to " + device.toString());
            final BluetoothSocket bluetoothSocket = device.createRfcommSocketToServiceRecord(TetheringServerService.UUID);
            bluetoothSocket.connect();
            Log.d(TAG, "connected to " + device.toString());
//            Toast.makeText(getApplicationContext(), "connected to " + device.toString(), Toast.LENGTH_SHORT).show();

            Log.d(TAG, "setting up Object{Input,Output}Stream");
            final ObjectOutputStream bluetoothOut = new ObjectOutputStream(bluetoothSocket.getOutputStream());
            bluetoothOut.flush();
            final ObjectInputStream bluetoothIn = new ObjectInputStream(bluetoothSocket.getInputStream());
            Log.d(TAG, "done setting up Object{Input,Output}Stream");

            final Builder builder = new Builder();
            builder.addRoute("0.0.0.0", 0);
            builder.addAddress("10.48.19.38", 32);

            Log.d(TAG, "establishing VPN");
            fileDescriptor = builder.establish();
            Log.d(TAG, "done establishing VPN");

            final FileInputStream in = new FileInputStream(fileDescriptor.getFileDescriptor());
            final FileOutputStream out = new FileOutputStream(fileDescriptor.getFileDescriptor());

            new Thread() {
                @Override
                public void run() {
                    try {
                        // TODO: Avoid busy wait when idle.
                        final byte[] packet = new byte[32767];
                        while (true) {
                            final int length = in.read(packet);

                            if (length > 0) {
                                Log.d(TAG, "read packet from VPN to bluetooth");
                                bluetoothOut.writeObject(Arrays.copyOfRange(packet, 0, length));
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "while writing to bluetooth", e);
                    } finally {
                        try {
                            fileDescriptor.close();
                            bluetoothSocket.close();
                        } catch (IOException e) {
                            // ignore
                        }
                    }
                }
            }.start();

            new Thread() {
                @Override
                public void run() {
                    try {
                        // TODO: Avoid busy wait when idle.
                        while (true) {
                            byte[] packet = (byte[]) bluetoothIn.readObject();
                            Log.d(TAG, "read packet from bluetooth to VPN");
                            out.write(packet, 0, packet.length);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "while reading from bluetooth", e);
                    } finally {
                        try {
                            fileDescriptor.close();
                            bluetoothSocket.close();
                        } catch (IOException e) {
                            // ignore
                        }
                    }
                }
            }.start();
        } catch (IOException e) {
            Log.wtf(TAG, e);
        }
    }

    @Override
    public void onDestroy() {
        if (fileDescriptor != null) {
            try {
                fileDescriptor.close();
            } catch (IOException e) {
                // ignore
            }
        }
        unregisterReceiver(broadcastReceiver);

        final SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putBoolean("pref_enable_client", false);
        editor.commit();
    }
}
