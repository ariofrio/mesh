package com.andresriofrio.mesh;

import android.app.IntentService;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class TetheringServerService extends Service {
    private static final String TAG = TetheringServerService.class.getName();
    public static final java.util.UUID UUID = java.util.UUID.fromString("b29f2a97-456d-47f8-8470-3318d10a326c");

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            Log.d(TAG, "starting tethering server service");
            final BluetoothServerSocket bluetoothServerSocket =
                    BluetoothAdapter.getDefaultAdapter().listenUsingRfcommWithServiceRecord("com.andresriofrio.mesh", UUID);

            new Thread() {
                @Override
                public void run() {

                    try {
//                        Toast.makeText(getApplicationContext(), "waiting for incoming bluetooth connection", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "waiting for incoming bluetooth connection");
                        final BluetoothSocket bluetoothSocket = bluetoothServerSocket.accept();
                        Log.d(TAG, "got incoming bluetooth connection");
//                        Toast.makeText(getApplicationContext(), "got incoming bluetooth connection", Toast.LENGTH_SHORT).show();

                        Log.d(TAG, "setting up Object{Input,Output}Stream");
                        final ObjectOutputStream out = new ObjectOutputStream(bluetoothSocket.getOutputStream());
                        out.flush();
                        final ObjectInputStream in = new ObjectInputStream(bluetoothSocket.getInputStream());
                        Log.d(TAG, "done setting up Object{Input,Output}Stream");

                        final IpTunnel tunnel = new IpTunnel();

                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    // TODO: Avoid busy wait when idle.
                                    while (true) {
                                        byte[] packet = (byte[]) in.readObject();
                                        tunnel.write(packet, 0, packet.length);
                                    }
                                } catch (Exception e) {
                                    Log.e(TAG, "while writing to tunnel from bluetooth", e);
                                } finally {
                                    try {
                                        bluetoothSocket.close();
                                        tunnel.close();
                                    } catch (IOException e) {
                                        // ignore
                                    }
                                }
                            }
                        }.start();

                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    // TODO: Avoid busy wait when idle.
                                    final byte[] packet = new byte[32767];
                                    while (true) {
                                        final int length = tunnel.read(packet);

                                        if (length > 0) {
                                            out.writeObject(Arrays.copyOfRange(packet, 0, length));
                                        }
                                    }
                                } catch (Exception e) {
                                    Log.e(TAG, "while reading from tunnel to bluetooth", e);
                                } finally {
                                    try {
                                        bluetoothSocket.close();
                                        tunnel.close();
                                    } catch (IOException e) {
                                        // ignore
                                    }
                                }
                            }
                        }.start();

                    } catch (Exception e) {
                        Log.e(TAG, "while writing to tunnel from bluetooth", e);
                    }
                }
            }.start();

        } catch (IOException e) {
            Log.wtf(TAG, e);
        }

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        final SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putBoolean("pref_enable_server", false);
        editor.commit();
    }
}
