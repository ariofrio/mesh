package com.andresriofrio.mesh;

import android.util.Log;

import com.andresriofrio.mesh.model.Ip4Packet;
import com.andresriofrio.mesh.model.TcpSegment;
import com.andresriofrio.mesh.model.UdpDatagram;

import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

public class IpTunnel {
    private static final String TAG = IpTunnel.class.getName();

    private ConcurrentHashMap<Integer, Thread> tcpOutsideSocketConnectionThreads = new ConcurrentHashMap<>();
    private ConcurrentHashMap<Integer, Socket> tcpOutsideSockets = new ConcurrentHashMap<>();
    private ConcurrentHashMap<Integer, InsideTcpConnection> tcpInsideConnections = new ConcurrentHashMap<>();

    private ConcurrentHashMap<Integer, DatagramSocket> udpOutsideSockets = new ConcurrentHashMap<>();

    private BlockingQueue<byte[]> readQueue = new ArrayBlockingQueue<>(32);

    public IpTunnel() {}

    /**
     * Read an incoming packet from the Internet. If no packet is available, block until one is.
     *
     * @param buffer    Buffer into which an IPv4 packet will be stored. Must be large enough to
     *                  fit the largest packet possible.
     * @return          The number of bytes written to the buffer.
     * @throws InterruptedException
     */
    public int read(byte[] buffer) throws InterruptedException {
        byte[] incoming = readQueue.take();

        final Ip4Packet packet = Ip4Packet.fromBytes(incoming, 0, incoming.length);
        Log.d(TAG, "responding with packet: " + packet.toString());

        System.arraycopy(incoming, 0, buffer, 0, incoming.length);
        return incoming.length;
    }

    /**
     * Write an outgoing packet to the Internet. This will also enqueue an ACK segment which
     * will be ...
     *
     * @param buffer        Buffer from which an IPv4 packet will be read.
     * @param byteOffset
     * @param byteCount
     * @throws java.io.IOException
     */
    public void write(byte[] buffer, int byteOffset, int byteCount) throws InterruptedException {
        final Ip4Packet packet = Ip4Packet.fromBytes(buffer, byteOffset, byteCount);
        if (packet.getProtocol() == 6) { // TCP
            processOutgoingTcpPacket(packet);
        } else if (packet.getProtocol() == 17) { // UDP
            processOutgoingUdpPacket(packet);
        } else {
            Log.d(TAG, "dropping non-UDP, non-TCP packet: " + packet.toString());
        }
    }

    private void processOutgoingUdpPacket(Ip4Packet packet) {
        UdpDatagram datagram = packet.getUdpDatagram();
        Log.d(TAG, "processing outgoing UDP datagram: " + packet.toString() + " " + datagram.toString());

        DatagramSocket socket = udpOutsideSockets.get(datagram.getSourcePort());
        if (socket == null) {
            try {
                socket = new DatagramSocket(datagram.getSourcePort());
            } catch (SocketException e) {
                Log.e(TAG, "while creating datagram socket for UDP datagram: " + packet.toString() + datagram.toString(), e);
                return;
            }
            udpOutsideSockets.put(datagram.getSourcePort(), socket);
            if (datagram.getSourcePort() != 0) {
                listenToDatagramSocket(socket);
            }
        }

        Log.d(TAG, "writing UDP datagram to socket: " + datagram.toString());
        try {
            socket.send(new DatagramPacket(
                    datagram.getDataBuffer(), datagram.getDataByteOffset(), datagram.getDataByteCount(),
                    packet.getDestinationIpAddress(), datagram.getDestinationPort()));
        } catch (IOException e) {
            Log.e(TAG, "while writing datagram to socket: " + datagram.toString(), e);
        }
    }

    private void processOutgoingTcpPacket(final Ip4Packet packet) throws InterruptedException {
        final TcpSegment segment = packet.getTcpSegment();

        if ((segment.getFlags() & 0x002) != 0) { // SYN
            Log.d(TAG, "processing outgoing SYN packet: " + packet.toString() + " " + segment.toString());
            if (tcpInsideConnections.get(segment.getSourcePort()) == null && tcpOutsideSocketConnectionThreads.get(segment.getSourcePort()) == null) {
                final InsideTcpConnection connection = makeInsideConnection(segment);
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        try {
                            Log.d(TAG, "about to create a socket for SYN packet: " + packet.toString() + " " + segment.toString());
                            try {
                                makeOutsideSocket(segment);
                            } catch (IOException e) {
                                Log.e(TAG, "while trying to create a socket for SYN packet: " + packet.toString() + " " + segment.toString(), e);
                                return;
                            }

                            Ip4Packet synAckPacket = connection.generateIncomingSynAckPacket();
                            TcpSegment synAckSegment = TcpSegment.fromBytes(
                                    synAckPacket.getDataBuffer(), synAckPacket.getDataByteOffset(), synAckPacket.getDataByteCount(),
                                    synAckPacket);
                            Log.d(TAG, "enqueuing SYN ACK packet: " + synAckPacket.toString() + " " + synAckSegment.toString());

                            try {
                                readQueue.put(synAckPacket.toBytes());
                            } catch (InterruptedException e) {
                                Log.e(TAG, "while waiting to add SYN ACK packet to the read queue: " + synAckPacket.toString() + " " + synAckSegment.toString(), e);
                                return;
                            }
                        } finally {
                            tcpOutsideSocketConnectionThreads.remove(Thread.currentThread());
                        }
                    }
                };
                tcpOutsideSocketConnectionThreads.put(segment.getSourcePort(), thread);
                thread.start();
            } else {
                Log.d(TAG, "dropped outgoing SYN packet: " + packet.toString() + " " + segment.toString());
            }
        } else {
            Log.d(TAG, "processing outgoing non-SYN packet: " + packet.toString() + " " + segment.toString());
            int sourcePort = segment.getSourcePort();
            InsideTcpConnection connection = tcpInsideConnections.get(sourcePort);
            Socket socket = tcpOutsideSockets.get(sourcePort);

            if (connection == null && socket == null) {
                Log.d(TAG, "dropping unassociated TCP packet: " + packet.toString() + " " + segment.toString());
            } else if (connection == null || socket == null) {
                Log.wtf(TAG, "only one of connection and socket is null, but not both");
            } else {
                Log.d(TAG, "writing TCP segment to socket: " + segment.toString());
                try {
                    socket.getOutputStream().write(
                            segment.getDataBuffer(), segment.getDataByteOffset(), segment.getDataByteCount());
                } catch (IOException e) {
                    Log.e(TAG, "while writing segment to socket: " + segment.toString(), e);
                    return;
                }

                if (connection.isEstablished()) {
                    final Ip4Packet ackPacket = connection.generateIncomingAckPacket();
                    TcpSegment ackSegment = TcpSegment.fromBytes(
                            ackPacket.getDataBuffer(), ackPacket.getDataByteOffset(), ackPacket.getDataByteCount(),
                            ackPacket);
                    Log.d(TAG, "enqueuing acknowledgement: " + ackPacket.toString() + " " + ackSegment.toString());
                    readQueue.put(ackPacket.toBytes());
                } else {
                    connection.processOutgoingDataPacket(segment);
                }
            }
        }
    }

    private InsideTcpConnection makeInsideConnection(TcpSegment outgoingSegment) {
        final int sourcePort = outgoingSegment.getSourcePort();
        InsideTcpConnection connection = new InsideTcpConnection(outgoingSegment);
        tcpInsideConnections.put(sourcePort, connection);
        return connection;
    }

    private Socket makeOutsideSocket(TcpSegment outgoingSegment) throws IOException {
        final int sourcePort = outgoingSegment.getSourcePort();
        Socket socket = new Socket(outgoingSegment.getDestinationIpAddress(), outgoingSegment.getDestinationPort());
        tcpOutsideSockets.put(sourcePort, socket);

        listenToSocket(sourcePort);
        return socket;
    }

    private void listenToDatagramSocket(final DatagramSocket socket) {new Thread() {
        @Override
        public void run() {
            try {
                final byte[] buffer = new byte[65535];
                while (true) {
                    DatagramPacket p = new DatagramPacket(buffer, buffer.length);
                    socket.receive(p);

                    Ip4Packet packet = new Ip4Packet();
                    UdpDatagram datagram = new UdpDatagram();

                    packet.setSourceIpAddress(p.getAddress());
                    packet.setDestinationIpAddress(Inet4Address.getByName("10.48.19.38"));

                    datagram.setSourcePort(p.getPort());
                    datagram.setDestinationPort(socket.getLocalPort());
                    datagram.setDataBuffer(p.getData());
                    datagram.setDataByteOffset(p.getOffset());
                    datagram.setDataByteCount(p.getLength());

                    packet.setUdpDatagram(datagram);

                    Log.d(TAG, "enqueuing incoming UDP datagram: " + packet.toString());
                    readQueue.put(packet.toBytes());
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }.start();
    }

    private void listenToSocket(final int sourcePort) throws IOException {
        final byte[] buffer = new byte[256];
        final Socket socket = tcpOutsideSockets.get(sourcePort);
        final InsideTcpConnection connection = tcpInsideConnections.get(sourcePort);
        final InputStream inputStream = socket.getInputStream();
        new Thread() {
            @Override
            public void run() {
                try {
                    while (true) {
                        final int length = inputStream.read(buffer);

                        if (length > 0) {
                            final Ip4Packet packet = connection.generateIncomingPacket(buffer, 0, length);
                            Log.d(TAG, "enqueuing incoming TCP data packet: " + packet.toString() + " " + packet.getTcpSegment().toString());
                            readQueue.put(packet.toBytes());
                        }
                    }
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void close() {
        // TODO: Close every socket.
    }
}
