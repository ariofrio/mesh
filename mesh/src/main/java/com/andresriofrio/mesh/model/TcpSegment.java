package com.andresriofrio.mesh.model;

import com.andresriofrio.mesh.util.Bits;
import com.andresriofrio.mesh.util.InternetChecksum;
import com.google.common.math.IntMath;

import java.math.RoundingMode;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class TcpSegment {

    private int sourcePort;
    private int destinationPort;
    private long sequenceNumber;
    private long acknowledgementNumber;

    private short flags;
    private int windowSize = 65535;

    private int urgentPointer;

    private byte[] optionsBuffer = new byte[0];
    private int optionsByteOffset = 0;
    private int optionsByteCount = 0;

    private byte[] dataBuffer = new byte[0];
    private int dataByteOffset = 0;
    private int dataByteCount = 0;

    private InetAddress sourceIpAddress;
    private InetAddress destinationIpAddress;

    public TcpSegment() {}

    public static TcpSegment fromBytes(byte[] buffer, int byteOffset, int byteCount, Ip4Packet packet) {
        return fromBytes(buffer, byteOffset, byteCount, packet.getSourceIpAddress(), packet.getDestinationIpAddress());
    }

    public static TcpSegment fromBytes(byte[] buffer, int byteOffset, int byteCount,
                                       InetAddress sourceIpAddress, InetAddress destinationIpAddress) {
        final TcpSegment segment = new TcpSegment();
        final ByteBuffer byteBuffer = ByteBuffer.wrap(buffer, byteOffset, byteCount);

        segment.sourceIpAddress = sourceIpAddress;
        segment.destinationIpAddress = destinationIpAddress;

        segment.sourcePort = Bits.asUnsigned(byteBuffer.getShort());
        segment.destinationPort = Bits.asUnsigned(byteBuffer.getShort());
        segment.sequenceNumber = Bits.asUnsigned(byteBuffer.getInt());
        segment.acknowledgementNumber = Bits.asUnsigned(byteBuffer.getInt());

        short dataOffsetAndFlags = byteBuffer.getShort();
        int headerWordCount = dataOffsetAndFlags >>> 12 & 0b1111;
        segment.flags = (short) (dataOffsetAndFlags & 0x01FF);

        segment.windowSize = Bits.asUnsigned(byteBuffer.getShort());

        // See http://en.wikipedia.org/wiki/Transmission_Control_Protocol#TCP_checksum_for_IPv4
        int checksum = Bits.asUnsigned(byteBuffer.getShort());
        segment.urgentPointer = Bits.asUnsigned(byteBuffer.getShort());

        segment.optionsByteCount = (headerWordCount - 5) * 4;
        segment.optionsBuffer = Arrays.copyOfRange(
                byteBuffer.array(), byteBuffer.position(), byteBuffer.position() + segment.optionsByteCount);
        segment.optionsByteOffset = 0;
        byteBuffer.position(byteBuffer.position() + segment.optionsByteCount);

        segment.dataBuffer = Arrays.copyOfRange(
                byteBuffer.array(), byteBuffer.position(), byteBuffer.limit());
        segment.dataByteOffset = 0;
        segment.dataByteCount = segment.dataBuffer.length;

        // Check sanity
        if (headerWordCount != segment.getHeaderWordCount()) throw new AssertionError();
        if (checksum != segment.getChecksum()) throw new AssertionError();

        return segment;
    }


    public int getSourcePort() {
        return sourcePort;
    }

    public int getDestinationPort() {
        return destinationPort;
    }

    public long getSequenceNumber() {
        return sequenceNumber;
    }

    public long getAcknowledgementNumber() {
        return acknowledgementNumber;
    }

    public int getHeaderWordCount() {
        return 5 + IntMath.divide(optionsByteCount, 4, RoundingMode.UP);
    }

    public long getChecksum() {
        return InternetChecksum.calculateChecksum(toBytes(true));
    }

    public int getUrgentPointer() {
        return urgentPointer;
    }

    public byte[] getDataBuffer() {
        return dataBuffer;
    }

    public int getDataByteOffset() {
        return dataByteOffset;
    }

    public int getDataByteCount() {
        return dataByteCount;
    }

    @Override
    public String toString() {
        return "TcpSegment{" +
                "sourcePort=" + sourcePort +
                ", destinationPort=" + destinationPort +
                ", sequenceNumber=" + sequenceNumber +
                ", acknowledgementNumber=" + acknowledgementNumber +
                ", flags=" + flags +
                ", windowSize=" + windowSize +
                ", urgentPointer=" + urgentPointer +
                ", optionsByteCount=" + optionsByteCount +
                ", dataByteCount=" + dataByteCount +
                ", sourceIpAddress=" + sourceIpAddress +
                ", destinationIpAddress=" + destinationIpAddress +
                '}';
    }

    public byte[] toBytes() {
        return toBytes(false);
    }

    /**
     * @param forChecksum Whether or not to use a zero checksum, pad the data at the end
     *                    to ensure that there is an integral number of 16-bit words, and prepend
     *                    an pseudo-header that mimics the IPv4 packet header.
     * @return
     */
    private byte[] toBytes(boolean forChecksum) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(dataByteCount + 60 + (forChecksum ? 24 : 0));

        if (forChecksum) {
            byteBuffer.put(sourceIpAddress.getAddress());
            byteBuffer.put(destinationIpAddress.getAddress());
            byteBuffer.put((byte) 0); // zeros
            byteBuffer.put((byte) 6); // protocol
            byteBuffer.putShort((short) getTotalByteCount());
        }

        byteBuffer.putShort((short) sourcePort);
        byteBuffer.putShort((short) destinationPort);
        byteBuffer.putInt((int) sequenceNumber);
        byteBuffer.putInt((int) acknowledgementNumber);
        byteBuffer.putShort((short) (getHeaderWordCount() << 12 | flags));
        byteBuffer.putShort((short) windowSize);
        byteBuffer.putShort((short) (forChecksum ? 0 : getChecksum()));
        byteBuffer.putShort((short) urgentPointer);

        byteBuffer.put(optionsBuffer, optionsByteOffset, optionsByteCount);
        int optionsPaddingByteCount = IntMath.mod(4 - optionsByteCount, 4); // TODO check accurate
        for (int i = 0; i < optionsPaddingByteCount; i++) {
            byteBuffer.put((byte) 0);
        }

        byteBuffer.put(dataBuffer, dataByteOffset, dataByteCount);

        if (forChecksum) {
            int dataPaddingByteCount = IntMath.mod(4 - dataByteCount, 4); // TODO check accurate
            for (int i = 0; i < dataPaddingByteCount; i++) {
                byteBuffer.put((byte) 0);
            }
        }

        return Arrays.copyOfRange(byteBuffer.array(), 0, byteBuffer.position());
    }

    private int getTotalByteCount() {
        return getHeaderWordCount() * 4 + getDataByteCount();
    }

    public short getFlags() {
        return flags;
    }

    public int getWindowSize() {
        return windowSize;
    }

    public void setSourcePort(int sourcePort) {
        this.sourcePort = sourcePort;
    }

    public void setDestinationPort(int destinationPort) {
        this.destinationPort = destinationPort;
    }

    public void setSequenceNumber(long sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public void setAcknowledgementNumber(long acknowledgementNumber) {
        this.acknowledgementNumber = acknowledgementNumber;
    }

    public void setFlags(short flags) {
        this.flags = flags;
    }

    public void setWindowSize(int windowSize) {
        this.windowSize = windowSize;
    }

    public void setUrgentPointer(int urgentPointer) {
        this.urgentPointer = urgentPointer;
    }

    public byte[] getOptionsBuffer() {
        return optionsBuffer;
    }

    public void setOptionsBuffer(byte[] optionsBuffer) {
        this.optionsBuffer = optionsBuffer;
    }

    public int getOptionsByteOffset() {
        return optionsByteOffset;
    }

    public void setOptionsByteOffset(int optionsByteOffset) {
        this.optionsByteOffset = optionsByteOffset;
    }

    public int getOptionsByteCount() {
        return optionsByteCount;
    }

    public void setOptionsByteCount(int optionsByteCount) {
        this.optionsByteCount = optionsByteCount;
    }

    public void setDataBuffer(byte[] dataBuffer) {
        this.dataBuffer = dataBuffer;
    }

    public void setDataByteOffset(int dataByteOffset) {
        this.dataByteOffset = dataByteOffset;
    }

    public void setDataByteCount(int dataByteCount) {
        this.dataByteCount = dataByteCount;
    }

    public InetAddress getSourceIpAddress() {
        return sourceIpAddress;
    }

    public void setSourceIpAddress(InetAddress sourceIpAddress) {
        this.sourceIpAddress = sourceIpAddress;
    }

    public InetAddress getDestinationIpAddress() {
        return destinationIpAddress;
    }

    public void setDestinationIpAddress(InetAddress destinationIpAddress) {
        this.destinationIpAddress = destinationIpAddress;
    }
}
