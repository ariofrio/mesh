package com.andresriofrio.mesh.model;

import com.andresriofrio.mesh.util.Bits;
import com.andresriofrio.mesh.util.InternetChecksum;
import com.google.common.math.IntMath;

import java.math.RoundingMode;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class Ip4Packet {
    private int ipVersion = 4;
    private int differentiatedServicesCodePoint = 0;
    private int explicitCongestionNotification = 0;
    private int storedIdentification = -1;              // replaced if necessary by getIdentification()
    private int flags = 0;
    private int fragmentOffset = 0;
    private short timeToLive = 128;
    private short protocol = 6;                         // TCP
    private InetAddress sourceIpAddress = null;         // bad!
    private InetAddress destinationIpAddress = null;    // bad!

    private byte[] optionsBuffer = new byte[0];
    private int optionsByteOffset = 0;
    private int optionsByteCount = 0;

    private byte[] dataBuffer = new byte[0];
    private int dataByteOffset = 0;
    private int dataByteCount = 0;

    public Ip4Packet() {}

    /**
     * Parse the given packet and store the header fields and the data into an Ip4Packet object.
     *
     * @param buffer        Buffer from which fields will be read. Data will be copied out of this
     *                      buffer, so it is safe to reuse after calling this method.
     * @param byteOffset
     * @param byteCount
     * @return
     */
    public static Ip4Packet fromBytes(byte[] buffer, int byteOffset, int byteCount) {
        final Ip4Packet packet = new Ip4Packet();
        final ByteBuffer byteBuffer = ByteBuffer.wrap(buffer, byteOffset, byteCount);

        // Separate components and check the version.
        byte versionAndHeaderWordCount = byteBuffer.get();
        packet.ipVersion = versionAndHeaderWordCount >>> 4;
        if (packet.ipVersion != 4) throw new IllegalArgumentException("Internet Protocol ipVersion is not 4");
        int headerWordCount = versionAndHeaderWordCount & 0x0F;

        byte dscpAndEcn = byteBuffer.get();
        packet.differentiatedServicesCodePoint = dscpAndEcn >>> 2;
        packet.explicitCongestionNotification = (dscpAndEcn << 6) >>> 6; // FIXME

        int totalByteCount = Bits.asUnsigned(byteBuffer.getShort());
        if (byteCount != totalByteCount) throw new IllegalArgumentException("provided byte count does not match total length within the packet");

        packet.storedIdentification = Bits.asUnsigned(byteBuffer.getShort());

        short flagsAndFragmentOffset = byteBuffer.getShort();
        packet.flags = flagsAndFragmentOffset >>> 13;
        packet.fragmentOffset = flagsAndFragmentOffset & 0x1FFF;

        packet.timeToLive = Bits.asUnsigned(byteBuffer.get());
        packet.protocol = Bits.asUnsigned(byteBuffer.get());
        int headerChecksum = Bits.asUnsigned(byteBuffer.getShort());

        byte[] sourceIpAddressBytes = new byte[4];
        byteBuffer.get(sourceIpAddressBytes);
        try {
            packet.sourceIpAddress = Inet4Address.getByAddress(sourceIpAddressBytes);
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }

        byte[] destinationIpAddressBytes = new byte[4];
        byteBuffer.get(destinationIpAddressBytes);
        try {
            packet.destinationIpAddress = Inet4Address.getByAddress(destinationIpAddressBytes);
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }

        packet.optionsByteCount = (headerWordCount - 5) * 4;
        packet.optionsBuffer = Arrays.copyOfRange(
                byteBuffer.array(), byteBuffer.position(), byteBuffer.position() + packet.optionsByteCount);
        packet.optionsByteOffset = 0;
        byteBuffer.position(byteBuffer.position() + packet.optionsByteCount);

        packet.dataBuffer = Arrays.copyOfRange(
                byteBuffer.array(), byteBuffer.position(), byteBuffer.limit());
        packet.dataByteOffset = 0;
        packet.dataByteCount = packet.dataBuffer.length;

        // Check sanity
        if (headerWordCount != packet.getHeaderWordCount()) throw new AssertionError();
        if (totalByteCount != packet.getTotalByteCount()) throw new AssertionError();
        if (headerChecksum != packet.getHeaderChecksum()) throw new AssertionError();

        return packet;
    }

    public int getIpVersion() {
        return ipVersion;
    }

    public int getHeaderWordCount() {
        return 5 + IntMath.divide(optionsByteCount, 4, RoundingMode.UP);
    }

    public int getDifferentiatedServicesCodePoint() {
        return differentiatedServicesCodePoint;
    }

    public int getExplicitCongestionNotification() {
        return explicitCongestionNotification;
    }

    public int getTotalByteCount() {
        return getHeaderWordCount() * 4 + dataByteCount;
    }

    public int getIdentification() {
        if (storedIdentification == -1) {
            storedIdentification = (int) (Math.random() * Integer.MAX_VALUE);
        }
        return storedIdentification;
    }

    public int getFlags() {
        return flags;
    }

    public int getFragmentOffset() {
        return fragmentOffset;
    }

    public int getTimeToLive() {
        return timeToLive;
    }

    public int getProtocol() {
        return protocol;
    }

    public long getHeaderChecksum() {
        return InternetChecksum.calculateChecksum(toBytes(true));
    }

    public InetAddress getSourceIpAddress() {
        return sourceIpAddress;
    }

    public InetAddress getDestinationIpAddress() {
        return destinationIpAddress;
    }

    public byte[] getOptionsBuffer() {
        return optionsBuffer;
    }

    public int getOptionsByteOffset() {
        return optionsByteOffset;
    }

    public int getOptionsByteCount() {
        return optionsByteCount;
    }

    public byte[] getDataBuffer() {
        return dataBuffer;
    }

    public int getDataByteOffset() {
        return dataByteOffset;
    }

    public int getDataByteCount() {
        return dataByteCount;
    }

    public void setIpVersion(int ipVersion) {
        this.ipVersion = ipVersion;
    }

    public void setDifferentiatedServicesCodePoint(int differentiatedServicesCodePoint) {
        this.differentiatedServicesCodePoint = differentiatedServicesCodePoint;
    }

    public void setExplicitCongestionNotification(int explicitCongestionNotification) {
        this.explicitCongestionNotification = explicitCongestionNotification;
    }

    public void setIdentification(int identification) {
        this.storedIdentification = identification;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    public void setFragmentOffset(int fragmentOffset) {
        this.fragmentOffset = fragmentOffset;
    }

    public void setTimeToLive(short timeToLive) {
        this.timeToLive = timeToLive;
    }

    public void setProtocol(short protocol) {
        this.protocol = protocol;
    }

    public void setSourceIpAddress(InetAddress sourceIpAddress) {
        this.sourceIpAddress = sourceIpAddress;
    }

    public void setDestinationIpAddress(InetAddress destinationIpAddress) {
        this.destinationIpAddress = destinationIpAddress;
    }

    public void setOptionsBuffer(byte[] optionsBuffer) {
        this.optionsBuffer = optionsBuffer;
    }

    public void setOptionsByteOffset(int optionsByteOffset) {
        this.optionsByteOffset = optionsByteOffset;
    }

    public void setOptionsByteCount(int optionsByteCount) {
        this.optionsByteCount = optionsByteCount;
    }

    public void setDataBuffer(byte[] dataBuffer) {
        this.dataBuffer = dataBuffer;
    }

    public void setDataByteOffset(int dataByteOffset) {
        this.dataByteOffset = dataByteOffset;
    }

    public void setDataByteCount(int dataByteCount) {
        this.dataByteCount = dataByteCount;
    }

    @Override
    public String toString() {
        String ret = "Ip4Packet{" +
                "ipVersion=" + ipVersion +
                ", headerWordCount=" + getHeaderWordCount() +
                ", differentiatedServicesCodePoint=" + differentiatedServicesCodePoint +
                ", explicitCongestionNotification=" + explicitCongestionNotification +
                ", totalByteCount=" + getTotalByteCount() +
                ", flags=" + flags +
                ", fragmentOffset=" + fragmentOffset +
                ", timeToLive=" + timeToLive +
                ", protocol=" + protocol +
                ", headerChecksum=" + getHeaderChecksum() +
                ", sourceIpAddress=" + sourceIpAddress +
                ", destinationIpAddress=" + destinationIpAddress +
                ", dataByteCount=" + dataByteCount;
        if (protocol == 6) { // TCP
            try {
                ret += ", data=" + getTcpSegment();
            } catch(Throwable e) {
                // ignore
            }
        } else if (protocol == 17) { // UDP
            try {
                ret += ", data=" + getUdpDatagram();
            } catch(Throwable e) {
                // ignore
            }
        }
        ret += '}';
        return ret;
    }

    /**
     * Format this packet according to the IPv4 packet structure.
     *
     * @return
     */
    public byte[] toBytes() {
        return toBytes(false);
    }

    /**
     *
     * @param forHeaderChecksum    Whether or not to use a zero checksum and skip the data.
     * @return
     */
    private byte[] toBytes(boolean forHeaderChecksum) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(dataByteCount + 60);

        byteBuffer.put((byte) (ipVersion << 4 | getHeaderWordCount()));
        byteBuffer.put((byte) (differentiatedServicesCodePoint << 2 | explicitCongestionNotification));
        byteBuffer.putShort((short) getTotalByteCount());
        byteBuffer.putShort((short) getIdentification());
        byteBuffer.putShort((short) (flags << 13 | fragmentOffset));
        byteBuffer.put((byte) timeToLive);
        byteBuffer.put((byte) protocol);
        byteBuffer.putShort((short) (forHeaderChecksum ? 0 : getHeaderChecksum()));
        byteBuffer.put(sourceIpAddress.getAddress());
        byteBuffer.put(destinationIpAddress.getAddress());

        byteBuffer.put(optionsBuffer, optionsByteOffset, optionsByteCount);
        int optionsPaddingByteCount = IntMath.mod(4 - optionsByteCount, 4); // TODO check accurate
        for (int i = 0; i < optionsPaddingByteCount; i++) {
            byteBuffer.put((byte) 0);
        }

        if (!forHeaderChecksum) {
            byteBuffer.put(dataBuffer, dataByteOffset, dataByteCount);
        }

        return Arrays.copyOfRange(byteBuffer.array(), 0, byteBuffer.position());
    }

    public TcpSegment getTcpSegment() {
        return TcpSegment.fromBytes(dataBuffer, dataByteOffset, dataByteCount, this);
    }

    public void setTcpSegment(TcpSegment segment) {
        protocol = 6; // TCP
        dataBuffer = segment.toBytes();
        dataByteOffset = 0;
        dataByteCount = dataBuffer.length;
    }

    public UdpDatagram getUdpDatagram() {
        return UdpDatagram.fromBytes(dataBuffer, dataByteOffset, dataByteCount);
    }

    public void setUdpDatagram(UdpDatagram datagram) {
        protocol = 17; // UDP
        dataBuffer = datagram.toBytes();
        dataByteOffset = 0;
        dataByteCount = dataBuffer.length;
    }

}
