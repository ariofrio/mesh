package com.andresriofrio.mesh.model;

import junit.framework.TestCase;

import org.apache.commons.codec.binary.Hex;

import java.util.Arrays;

public class Ip4PacketTest extends TestCase {

    private static final String TAG = Ip4PacketTest.class.getName();

    public void testPacketFromBytes() throws Exception {
        byte[] buffer = Hex.decodeHex("45000034195f40008006c10fa9e73c604a7def90c5a3005008c7704a0000000080022000efb50000020405b40103030801010402".toCharArray());
        Ip4Packet packet = Ip4Packet.fromBytes(buffer, 0, buffer.length);
        assertEquals(4, packet.getIpVersion());
        assertEquals(5, packet.getHeaderWordCount());
//        assertEquals(20, packet.getHeaderByteCount());
        assertEquals(0, packet.getDifferentiatedServicesCodePoint());
        assertEquals(0, packet.getExplicitCongestionNotification());
        assertEquals(52, packet.getTotalByteCount());
        assertEquals(0x195f, packet.getIdentification());
        assertEquals(0b010, packet.getFlags());
        assertEquals(0, packet.getFragmentOffset());
        assertEquals(128, packet.getTimeToLive());
        assertEquals(6, packet.getProtocol());
        assertEquals(0xc10f, packet.getHeaderChecksum());
        assertEquals("169.231.60.96", packet.getSourceIpAddress().getHostAddress());
        assertEquals("74.125.239.144", packet.getDestinationIpAddress().getHostAddress());
        // Also check options?
        assertTrue(Arrays.equals(Hex.decodeHex("c5a3005008c7704a0000000080022000efb50000020405b40103030801010402".toCharArray()),
                Arrays.copyOfRange(packet.getDataBuffer(), packet.getDataByteOffset(), packet.getDataByteOffset() + packet.getDataByteCount())));
    }

    public void testPacketRoundTripConversion() throws Exception {
        final String hex = "45000034195f40008006c10fa9e73c604a7def90c5a3005008c7704a0000000080022000efb50000020405b40103030801010402";
        byte[] buffer = Hex.decodeHex(hex.toCharArray());
        Ip4Packet packet = Ip4Packet.fromBytes(buffer, 0, buffer.length);
        assertEquals(hex, new String(Hex.encodeHex(packet.toBytes())));
    }

}
