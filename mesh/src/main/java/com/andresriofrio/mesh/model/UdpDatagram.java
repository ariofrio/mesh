package com.andresriofrio.mesh.model;

import com.andresriofrio.mesh.util.Bits;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class UdpDatagram {

    private int sourcePort;
    private int destinationPort;

    private byte[] dataBuffer = new byte[0];
    private int dataByteOffset = 0;
    private int dataByteCount = 0;

    public static UdpDatagram fromBytes(byte[] buffer, int byteOffset, int byteCount) {
        UdpDatagram datagram = new UdpDatagram();
        final ByteBuffer byteBuffer = ByteBuffer.wrap(buffer, byteOffset, byteCount);

        datagram.sourcePort = Bits.asUnsigned(byteBuffer.getShort());
        datagram.destinationPort = Bits.asUnsigned(byteBuffer.getShort());
        int totalByteCount = Bits.asUnsigned(byteBuffer.getShort());
        int checksum = Bits.asUnsigned(byteBuffer.getShort()); // ignored

        datagram.dataBuffer = Arrays.copyOfRange(
                byteBuffer.array(), byteBuffer.position(), byteBuffer.limit());
        datagram.dataByteOffset = 0;
        datagram.dataByteCount = datagram.dataBuffer.length;

        // check sanity
        if (totalByteCount != datagram.getTotalByteCount()) throw new AssertionError();

        return datagram;
    }

    public byte[] toBytes() {
        ByteBuffer byteBuffer = ByteBuffer.allocate(getTotalByteCount());

        byteBuffer.putShort((short) sourcePort);
        byteBuffer.putShort((short) destinationPort);
        byteBuffer.putShort((short) getTotalByteCount());
        byteBuffer.putShort((short) 0);

        byteBuffer.put(dataBuffer, dataByteOffset, dataByteCount);

        return Arrays.copyOfRange(byteBuffer.array(), 0, byteBuffer.position());
    }

    private int getTotalByteCount() {
        return 8 + dataByteCount;
    }

    public int getSourcePort() {
        return sourcePort;
    }

    public void setSourcePort(int sourcePort) {
        this.sourcePort = sourcePort;
    }

    public int getDestinationPort() {
        return destinationPort;
    }

    public void setDestinationPort(int destinationPort) {
        this.destinationPort = destinationPort;
    }

    public byte[] getDataBuffer() {
        return dataBuffer;
    }

    public void setDataBuffer(byte[] dataBuffer) {
        this.dataBuffer = dataBuffer;
    }

    public int getDataByteOffset() {
        return dataByteOffset;
    }

    public void setDataByteOffset(int dataByteOffset) {
        this.dataByteOffset = dataByteOffset;
    }

    public int getDataByteCount() {
        return dataByteCount;
    }

    public void setDataByteCount(int dataByteCount) {
        this.dataByteCount = dataByteCount;
    }

    @Override
    public String toString() {
        return "UdpDatagram{" +
                "sourcePort=" + sourcePort +
                ", destinationPort=" + destinationPort +
                ", dataByteCount=" + dataByteCount +
                '}';
    }
}
