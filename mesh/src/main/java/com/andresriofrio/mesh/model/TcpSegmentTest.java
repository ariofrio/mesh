package com.andresriofrio.mesh.model;

import junit.framework.TestCase;

import org.apache.commons.codec.binary.Hex;

import java.net.InetAddress;

public class TcpSegmentTest extends TestCase {

    public void testSegmentFromBytes() throws Exception {
        byte[] segmentBuffer = Hex.decodeHex("c5a3005008c7704a0000000080022000efb50000020405b40103030801010402".toCharArray());
        TcpSegment segment = TcpSegment.fromBytes(segmentBuffer, 0, segmentBuffer.length,
                InetAddress.getByName("169.231.60.96"), InetAddress.getByName("74.125.239.144"));
        assertEquals(50595, segment.getSourcePort());
        assertEquals(80, segment.getDestinationPort());
        assertEquals(0x08c7704aL, segment.getSequenceNumber());
        assertEquals(0, segment.getAcknowledgementNumber());
        assertEquals(8, segment.getHeaderWordCount());
        assertEquals(0x002, segment.getFlags());
        assertEquals(8192, segment.getWindowSize());
        assertEquals(0xefb5, segment.getChecksum());
        // options?
        assertEquals(0, segment.getDataByteCount());
    }

    public void testSegmentRoundTripConversion() throws Exception {
        final String hex = "c5a3005008c7704a0000000080022000efb50000020405b40103030801010402";
        byte[] buffer = Hex.decodeHex(hex.toCharArray());
        TcpSegment segment = TcpSegment.fromBytes(buffer, 0, buffer.length,
                InetAddress.getByName("169.231.60.96"), InetAddress.getByName("74.125.239.144"));
        assertEquals(hex, new String(Hex.encodeHex(segment.toBytes())));
    }

}
