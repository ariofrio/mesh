package com.andresriofrio.mesh;

import com.andresriofrio.mesh.model.Ip4Packet;
import com.andresriofrio.mesh.model.TcpSegment;

import java.net.InetAddress;
import java.util.Random;

public class InsideTcpConnection {
    private boolean established = false;

    public boolean isEstablished() {
        return established;
    }

    private long incomingAcknowledgementNumber = -1;
    private long incomingSequenceNumber = -1;

    private final InetAddress insideIpAddress;
    private final int insidePort;
    private final InetAddress outsideIpAddress;
    private final int outsidePort;

    public InsideTcpConnection(InetAddress insideIpAddress, int insidePort, InetAddress outsideIpAddress, int outsidePort) {
        this.insideIpAddress = insideIpAddress;
        this.insidePort = insidePort;
        this.outsideIpAddress = outsideIpAddress;
        this.outsidePort = outsidePort;
    }

    public InsideTcpConnection(TcpSegment outgoingSynSegment) {
        if ((outgoingSynSegment.getFlags() & 0x002) == 0) { // SYN
            throw new IllegalArgumentException("argument must be a SYN segment");
        }

        this.insideIpAddress = outgoingSynSegment.getSourceIpAddress();
        this.insidePort = outgoingSynSegment.getSourcePort();
        this.outsideIpAddress = outgoingSynSegment.getDestinationIpAddress();
        this.outsidePort = outgoingSynSegment.getDestinationPort();

        this.incomingAcknowledgementNumber = outgoingSynSegment.getSequenceNumber() + 1;
        this.incomingSequenceNumber = (long) (Math.random() * Long.MAX_VALUE);
    }

    public void setIncomingAcknowledgementNumber(long incomingAcknowledgementNumber) {
        this.incomingAcknowledgementNumber = incomingAcknowledgementNumber;
    }

    public long getIncomingAcknowledgementNumber() {
        return incomingAcknowledgementNumber;
    }

    public long getIncomingSequenceNumber() {
        return incomingSequenceNumber;
    }

    public void processOutgoingDataPacket(TcpSegment outgoingSegment) {
        long tentativeIncomingAcknowledgementNumber = outgoingSegment.getSequenceNumber() + outgoingSegment.getDataByteCount();
        if (tentativeIncomingAcknowledgementNumber > incomingAcknowledgementNumber) {
            incomingAcknowledgementNumber = tentativeIncomingAcknowledgementNumber;
        }
    }

    /**
     *
     * @param outgoingAckSegment
     * @return true if the packet was processed or false if the packet was dropped
     */
    public boolean processOutgoingAckPacket(TcpSegment outgoingAckSegment) {
        if ((outgoingAckSegment.getFlags() & 0x010) == 0) { // ACK
            throw new IllegalArgumentException("argument must be a ACK segment");
        }

        if (!established) {
            if (incomingSequenceNumber == outgoingAckSegment.getAcknowledgementNumber()) {
                established = true;
                return true;
            } else {
                return false;
            }
        } else {
            return false; // TODO?
        }
    }

    public Ip4Packet generateIncomingSynAckPacket() {
        Ip4Packet packet = generateIncomingAckPacket();
        TcpSegment segment = packet.getTcpSegment();
        segment.setFlags((short) (segment.getFlags() | 0x002)); // SYN
        packet.setTcpSegment(segment);

        incomingSequenceNumber += 1;
        return packet;
    }

    public Ip4Packet generateIncomingAckPacket() {
        Ip4Packet packet = new Ip4Packet();
        TcpSegment segment = new TcpSegment();

        packet.setDestinationIpAddress(insideIpAddress);
        packet.setSourceIpAddress(outsideIpAddress);

        segment.setDestinationIpAddress(insideIpAddress);
        segment.setDestinationPort(insidePort);
        segment.setSourceIpAddress(outsideIpAddress);
        segment.setSourcePort(outsidePort);

        segment.setSequenceNumber(incomingSequenceNumber);
        segment.setAcknowledgementNumber(incomingAcknowledgementNumber);
        segment.setFlags((short) 0x010); // ACK

        packet.setDataBuffer(segment.toBytes());
        packet.setDataByteOffset(0);
        packet.setDataByteCount(packet.getDataBuffer().length);

        return packet;
    }


    public Ip4Packet generateIncomingPacket(byte[] buffer, int byteOffset, int byteCount) {
        Ip4Packet packet = new Ip4Packet();
        TcpSegment segment = new TcpSegment();

        packet.setDestinationIpAddress(insideIpAddress);
        packet.setSourceIpAddress(outsideIpAddress);

        segment.setDestinationIpAddress(insideIpAddress);
        segment.setDestinationPort(insidePort);
        segment.setSourceIpAddress(outsideIpAddress);
        segment.setSourcePort(outsidePort);

        segment.setSequenceNumber(incomingSequenceNumber);
        segment.setAcknowledgementNumber(incomingAcknowledgementNumber);
        segment.setFlags((short) 0x010); // ACK

        // We are reusing the buffer, but segment.toBytes() will copy the data out of the buffer.
        segment.setDataBuffer(buffer);
        segment.setDataByteOffset(byteOffset);
        segment.setDataByteCount(byteCount);

        incomingSequenceNumber += segment.getDataByteCount();

        packet.setDataBuffer(segment.toBytes());
        packet.setDataByteOffset(0);
        packet.setDataByteCount(packet.getDataBuffer().length);

        return packet;
    }
}
